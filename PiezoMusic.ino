#define IN  1
long int freq=0;
long int lastint=0;

void setup() {
  pinMode(3, OUTPUT);
  pinMode(12, INPUT);
  // Serial.begin(9600);
}

void loop() {
  freq=analogRead(IN) * 2 + 31;
  if(digitalRead(12) == HIGH)
  {
    tone(3, freq);
    if(freq != lastint)
    {
/*      Serial.print("\r");
      Serial.print("FREQ=");
      Serial.print(freq);
      Serial.print("   "); */
      lastint=freq;
    }
  }
  else
  {
    noTone(3);
  }
}
